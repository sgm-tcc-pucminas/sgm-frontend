var urlSeguranca = "http://34.95.188.228:8001/";
var urlServicos = "http://35.199.94.178:8006/";
var logadoComSucesso = false;

var nomeUsuario = "Usuario";
var perfil = "ANONIMO";
var token = "";

var escolas;
var escolaSelecionada;
var servicoSelecionado;

var grid;
var listaVagas;
var listaServicos;
var listaSolicitacoes;
var listaResponsavel;

$(function () {
    inicializarTela();
    habilitarCamposPublicos();

    if (logadoComSucesso) {
        token = sessionStorage.getItem("token");
        nomeUsuario = sessionStorage.getItem("nomeUsuario");
        perfil = sessionStorage.getItem("perfil");
        habilitarCamposPrivados();
    }

    $('#btn-fazer-login').on('click', function (e) {
        habilitarTelaLogin();
    });

    //botoes de acesso
    $('#continuar-sem-login').on('click', function (e) {
        logadoComSucesso = false;
        habilitarCamposPublicos();
    });

    $('#btn-login').on('click', function (e) {
        let username = $('#login-username').val();
        let password = $('#login-password').val();

        if (username == undefined) {
            alert("Informe seu nome de usuário");

        } else if (password == undefined) {
            alert("Informe sua senha");

        } else {
            fazerLoginUsuario(username, password);
        }
    });

    $('#btn-logout').on('click', function (e) {
        logadoComSucesso = false;
        sessionStorage.clear();

        $('#janela-login').show();
        $('#dados-usuario').hide();

        $('#linha-secretarias').hide();
        $('#grid_vagas').hide();
        $('#grid_servicos').hide();
        $('#grid-vaga-protocolo').hide();
        $('#grid-servico-protocolo').hide();
        $('#cadastro_nova_vaga_form').hide();
        $('#aprovar_novo_servico_form').hide();
    });


    //botoes menu secretaria educacao
    $('#btn-vagas').on('click', function (e) {
        obterVagasDisponiveis();

        $('#grid_vagas').show();
        $('#grid-vaga-protocolo').hide();
        $('#grid-servico-protocolo').hide();
        $('#cadastro_nova_vaga_form').hide();

        $('#grid_servicos').hide();
        $('#cadastro_novo_servico_form').hide();
        $('#solicitar_servico_form').hide();
        $('#aprovar_novo_servico_form').hide();
    });

    $('#btn-criar-nova-vaga').on('click', function (e) {
        obterEscolas();

        $('#grid_vagas').hide();
        $('#cadastro_nova_vaga_form').show();
        $('#grid-vaga-protocolo').hide();
        $('#grid-servico-protocolo').hide();

        $('#grid_servicos').hide();
        $('#cadastro_novo_servico_form').hide();
        $('#solicitar_servico_form').hide();
        $('#aprovar_novo_servico_form').hide();
    });

    $('#btn-solicitacao-vagas-pendentes').on('click', function (e) {
        obterSolicitacaoPendente();

        $('#grid_vagas').show();
        $('#cadastro_nova_vaga_form').hide();
        $('#grid-vaga-protocolo').hide();
        $('#grid-servico-protocolo').hide();

        $('#grid_servicos').hide();
        $('#cadastro_novo_servico_form').hide();
        $('#solicitar_servico_form').hide();
        $('#aprovar_novo_servico_form').hide();
    });


    $('#btn-solicitacao-vaga-protocolo').on('click', function (e) {
        limparPesquisaProtocolo();

        $('#grid_vagas').hide();
        $('#grid-vaga-protocolo').show();
        $('#grid-servico-protocolo').hide();
        $('#cadastro_nova_vaga_form').hide();

        $('#grid_servicos').hide();
        $('#cadastro_novo_servico_form').hide();
        $('#solicitar_servico_form').hide();
        $('#aprovar_novo_servico_form').hide();
    });

    $('#btn-minhas-vagas').on('click', function (e) {
        obterMinhasSolicitacoes();

        $('#grid_vagas').show();
        $('#grid-vaga-protocolo').hide();
        $('#grid-servico-protocolo').hide();
        $('#cadastro_nova_vaga_form').hide();

        $('#grid_servicos').hide();
        $('#cadastro_novo_servico_form').hide();
        $('#solicitar_servico_form').hide();
        $('#aprovar_novo_servico_form').hide();
    });

    //botoes menu secretaria servicos
    $('#btn-servicos').on('click', function (e) {
        obterServicosDisponiveis();

        $('#grid_vagas').hide();
        $('#grid-vaga-protocolo').hide();
        $('#grid-servico-protocolo').hide();
        $('#cadastro_nova_vaga_form').hide();

        $('#grid_servicos').show();
        $('#cadastro_novo_servico_form').hide();
        $('#solicitar_servico_form').hide();
        $('#aprovar_novo_servico_form').hide();
    });

    $('#btn-novo-servico').on('click', function (e) {
        $('#grid_vagas').hide();
        $('#grid-vaga-protocolo').hide();
        $('#grid-servico-protocolo').hide();
        $('#cadastro_nova_vaga_form').hide();

        $('#grid_servicos').hide();
        $('#cadastro_novo_servico_form').show();
        $('#solicitar_servico_form').hide();
        $('#aprovar_novo_servico_form').hide();
    });

    $('#btn-solicitacoes-servico-pendentes').on('click', function (e) {
        obterSolicitacaoServicoPendente();

        $('#grid_vagas').hide();
        $('#grid-vaga-protocolo').hide();
        $('#grid-servico-protocolo').hide();
        $('#cadastro_nova_vaga_form').hide();

        $('#grid_servicos').show();
        $('#cadastro_novo_servico_form').hide();
        $('#solicitar_servico_form').hide();
        $('#aprovar_novo_servico_form').hide();
    });

    $('#btn-solicitacao-servico-protocolo').on('click', function (e) {
        limparPesquisaProtocoloServico();

        $('#grid_vagas').hide();
        $('#cadastro_nova_vaga_form').hide();

        $('#grid_servicos').hide();
        $('#grid-vaga-protocolo').hide();
        $('#grid-servico-protocolo').show();
        $('#cadastro_novo_servico_form').hide();
        $('#solicitar_servico_form').hide();
        $('#aprovar_novo_servico_form').hide();
    });

    $('#btn-meus-servicos').on('click', function (e) {
        obterMinhasSolicitacoesServico();

        $('#grid_vagas').hide();
        $('#grid-vaga-protocolo').hide();
        $('#grid-servico-protocolo').hide();
        $('#cadastro_nova_vaga_form').hide();

        $('#grid_servicos').show();
        $('#cadastro_novo_servico_form').hide();
        $('#solicitar_servico_form').hide();
        $('#aprovar_novo_servico_form').hide();
    });


    $('#pesquisar-protocolo-vaga').on('click', function (e) {
        let protocolo = $('#numero-protocolo-solicitacao').focusout().val();
        pesquisarSolicitacaoProtocolo(protocolo);
    });

    $('#pesquisar-protocolo-servico').on('click', function (e) {
        let protocolo = $('#numero-protocolo-solicitacao-servicos').focusout().val();
        pesquisarSolicitacaoProtocoloServico(protocolo);
    });

    $('#btn-solicitar-vaga').on('click', function (e) {

        if (!logadoComSucesso) {
            habilitarTelaLogin();

        } else {
            alert('falta implementar');
        }
    });

    $('#solicitar-servicos').on('click', function (e) {

        if (!logadoComSucesso) {
            habilitarTelaLogin();

        } else {
            alert('falta implementar');
        }
    });

    $('#cadastrar-vaga').on('click', function (e) {
        cadastrarNovaVaga();
    });

    $('#cadastrar-servico').on('click', function (e) {
        cadastrarNovoServico();
    });

    $('#escola_lista').change(function () {
        let idEscola = ($(this).val());

        for (i = 0; i < escolas.length; i++) {

            if (escolas[i].id == idEscola) {
                escolaSelecionada = escolas[i];

                $('#escola_endereco').val(escolaSelecionada.endereco);
                $('#escola_bairro').val(escolaSelecionada.bairro);
                $('#escola_cep').val(escolaSelecionada.cep);
                $('#escola_contato').val(escolaSelecionada.contato);
                $('#escola_responsavel').val(escolaSelecionada.responsavel);
            }
        }
    });

    $('#btn-solicitar-servico').on('click', function (e) {
        enviarSolicitacaoServico();
    });

    $('#btn-aprovar-solicitacao-servico').on('click', function (e) {
        aprovarSolicitacaoServico();
    });
});

$(document).on('click', '#grid-vagas-tabela .btn', function () {
    let id = ($(this).val());

    if (grid === "SOLICITACOES") {
        aprovarSolicitao(id);
    }

    if (grid === "VAGAS") {

        if (logadoComSucesso) {
            solicitarVaga(id);

        } else {
            habilitarTelaLogin();
        }
    }
});

$(document).on('click', '#grid-servicos-tabela .btn', function () {
    let id = ($(this).val());

    if (grid === "SOLICITACOES_SERVICO") {
        aprovarSolicitaoServico(id);
    }

    if (grid === "SERVICOS") {

        if (logadoComSucesso) {
            solicitarServico(id);

        } else {
            habilitarTelaLogin();
        }
    }
});

function inicializarTela() {
    $('#grid_vagas').hide();
    $('#grid-vaga-protocolo').hide();
    $('#grid-servico-protocolo').hide();
    $('#cadastro_nova_vaga_form').hide();
    $('#grid_servicos').hide();
    $('#aprovar_novo_servico_form').hide();
    $('#cadastro_novo_servico_form').hide();
    $('#solicitar_servico_form').hide();
    $('#janela-login').hide();

    logadoComSucesso = sessionStorage.getItem("logadoComSucesso");
}

function habilitarCamposPublicos() {
    $('#linha-secretarias').show();
    $('#btn-fazer-login').show();

    $('#janela-login').hide();
    $('#dados-usuario').hide();

    //botoes educacao
    $('#btn-criar-nova-vaga').hide();
    $('#btn-solicitacao-vagas-pendentes').hide();
    $('#btn-solicitacao-vaga-protocolo').hide();
    $('#btn-minhas-vagas').hide();

    //botoes servicos
    $('#btn-novo-servico').hide();
    $('#btn-solicitacoes-servico-pendentes').hide();
    $('#btn-solicitacao-servico-protocolo').hide();
    $('#btn-meus-servicos').hide();
}

function habilitarCamposPrivados() {
    $('#dados-usuario').show();
    $('#linha-secretarias').show();
    $('#btn-fazer-login').hide();
    $('#janela-login').hide();

    if (isAdministrativo() || isSuport()) {
        $('#btn-criar-nova-vaga').show();
        $('#btn-solicitacao-vagas-pendentes').show();
        $('#btn-solicitacao-vaga-protocolo').show();
        $('#btn-minhas-vagas').hide();

        $('#btn-novo-servico').show();
        $('#btn-solicitacoes-servico-pendentes').show();
        $('#btn-solicitacao-servico-protocolo').show();
        $('#btn-meus-servicos').hide();

    } else {
        $('#btn-criar-nova-vaga').hide();
        $('#btn-solicitacao-vagas-pendentes').hide();
        $('#btn-solicitacao-vaga-protocolo').show();
        $('#btn-minhas-vagas').show();

        $('#btn-novo-servico').hide();
        $('#btn-solicitacoes-servico-pendentes').hide();
        $('#btn-solicitacao-servico-protocolo').show();
        $('#btn-meus-servicos').show();
    }

    habilitarCamposUsuario();
}

function habilitarTelaLogin() {
    $('#linha-secretarias').hide();
    $('#grid_vagas').hide();
    $('#grid-vaga-protocolo').hide();
    $('#grid-servico-protocolo').hide();
    $('#grid_servicos').hide();
    $('#aprovar_novo_servico_form').hide();
    $('#cadastro_nova_vaga_form').hide();
    $('#cadastro_novo_servico_form').hide();
    $('#solicitar_servico_form').hide();

    $('#dados-usuario').hide();
    $('#btn-fazer-login').hide();
    $('#janela-login').show();
}

function fazerLoginUsuario(username, password) {
    $.ajax({
        url: urlSeguranca + "oauth/token",
        method: "POST",
        dataType: "json",
        data: { grant_type: 'password', username: username, password: password },
        headers: {
            "Authorization": "Basic " + btoa("sgm-tcc-pucminas:sgm-tcc@pucminas")
        },
        success: function (data) {
            token = data.access_token;
            nomeUsuario = data.nome;
            perfil = data.authorities[0];

            sessionStorage.setItem("token", token);
            sessionStorage.setItem("nomeUsuario", nomeUsuario);
            sessionStorage.setItem("perfil", perfil);
            sessionStorage.setItem("logadoComSucesso", true);

            logadoComSucesso = true;
            habilitarCamposPrivados();

        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Usuário ou senha inválida");
        }
    });
}

function habilitarCamposUsuario() {
    $('#nome-usuario').text(nomeUsuario);
    $('#janela-login').hide();
    $('#dados-usuario').show();
    $('#linha-secretarias').show();
}


//INICIO - EDUCACAO

function cadastrarNovaVaga() {
    let vaga = {
        idescola: escolaSelecionada.id,
        nome: escolaSelecionada.nome,
        logradouro: escolaSelecionada.endereco,
        bairro: escolaSelecionada.bairro,
        cep: escolaSelecionada.cep
    }

    $.ajax({
        url: urlServicos + "api/administrative/v1/educacao/vaga",
        method: "POST",
        dataType: "json",
        data: JSON.stringify(vaga),
        headers: {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        },
        success: function (data) {
            escolaSelecionada = null;

            $('#escola_endereco').val("");
            $('#escola_bairro').val("");
            $('#escola_cep').val("");
            $('#escola_contato').val("");
            $('#escola_responsavel').val("");

            inicializarTela();

        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Erro ao cadastrar vaga");
        }
    });
}

function solicitarVaga(id) {
    let vaga = { idvaga: id };

    $.ajax({
        url: urlServicos + "api/private/v1/educacao/vaga",
        method: "POST",
        dataType: "json",
        data: JSON.stringify(vaga),
        headers: {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        },
        success: function (data) {
            inicializarTela();

        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Erro ao cadastrar vaga");
        }
    });
}

function aprovarSolicitao(id) {
    $.ajax({
        url: urlServicos + "api/administrative/v1/educacao/vaga/solicitacao/" + id,
        method: "PUT",
        dataType: "json",
        headers: {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        },
        success: function (data) {
            inicializarTela();

        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Erro ao aprovar solicitacao");
        }
    });
}

function obterVagasDisponiveis() {

    $.getJSON(urlServicos + "api/public/v1/educacao/vaga", function (result, status, xhr) {

        if (status == 'success') {
            montarListaVagas(result);

        } else {
            $('#grid-vagas-tabela tr').remove();
        }
    });
}

function montarListaVagas(vagas) {
    grid = "VAGAS";

    listaVagas = vagas.vagas;
    $('#grid-vagas-tabela tr').remove();
    linha = '<thead>';
    linha += '<tr>';
    linha += '<th>Escola</th>';
    linha += '<th>Endereço</th>';
    linha += '<th>Contato</th>';
    linha += '<th>Responsável</th>';

    if (!isAdministrativo()) {
        linha += '<th></th>';
    }

    linha += '</tr>';
    linha += '</thead>';
    $('#grid-vagas-tabela').append(linha);

    for (i = 0; i < listaVagas.length; i++) {
        linha = '<tbody>';
        linha += "<tr>";
        linha += '<tr>';
        linha += '<td>' + listaVagas[i].nome + '</td>';
        linha += '<td>' + listaVagas[i].logradouro + ', ' + listaVagas[i].bairro + ' - ' + listaVagas[i].cep + '</td>';
        linha += '<td>' + listaVagas[i].contato + '</td>';
        linha += '<td>' + listaVagas[i].responsavel + '</td>';

        if (!isAdministrativo()) {
            linha += '<td><button value="' + listaVagas[i].id + '" class="btn btn-primary btn-user btn-block">Solicitar</button></td>';
        }

        linha += '</tr>';
        $('#grid-vagas-tabela').append(linha);
    }

    linha = '</tbody>';
    $('#grid-vagas-tabela').append(linha);
}

function obterSolicitacaoPendente() {
    $.ajaxSetup({ headers: { 'Authorization': 'Bearer ' + token } });
    $.getJSON(urlServicos + "api/administrative/v1/educacao/vaga/solicitacao/1", function (result, status, xhr) {

        if (status == 'success') {
            montarListaSolicitacoes(result);

        } else {
            $('#grid-vagas-tabela tr').remove();
        }
    });
}

function obterEscolas() {
    $.ajaxSetup({ headers: { 'Authorization': 'Bearer ' + token } });
    $.getJSON(urlServicos + "api/administrative/v1/educacao/escolas", function (result, status, xhr) {
        escolas = result.escolas;
        montarOpcoesEscola();
    });
}

function montarOpcoesEscola() {
    $('#escola_lista option').remove();

    let linha = '<option value="0">Selecione uma escola</option>';

    for (i = 0; i < escolas.length; i++) {
        linha += '<option value="' + escolas[i].id + '">' + escolas[i].nome + '</option>';

    }

    $('#escola_lista').append(linha);
}

function montarListaSolicitacoes(result) {
    grid = "SOLICITACOES";

    listaSolicitacoes = result.solicitacoes;
    $('#grid-vagas-tabela tr').remove();

    linha = '<thead>';
    linha += '<tr>';
    linha += '<th>Escola</th>';
    linha += '<th>Endereço</th>';
    linha += '<th>Solicitante</th>';
    linha += '<th>Contato</th>';
    linha += '<th></th>';
    linha += '</tr>';
    linha += '</thead>';

    $('#grid-vagas-tabela').append(linha);

    for (i = 0; i < listaSolicitacoes.length; i++) {
        linha = '<tbody>';
        linha += "<tr>";
        linha += '<tr>';
        linha += '<td>' + listaSolicitacoes[i].vaga.nome + '</td>';
        linha += '<td>' + listaSolicitacoes[i].vaga.logradouro + ', ' + listaSolicitacoes[i].vaga.bairro + ' - ' + listaSolicitacoes[i].vaga.cep + '</td>';
        linha += '<td>' + listaSolicitacoes[i].usuariosolicitacao.nome + '</td>';
        linha += '<td>' + listaSolicitacoes[i].usuariosolicitacao.telefone + '</td>';
        linha += '<td><button value="' + listaSolicitacoes[i].id + '" class="btn btn-primary btn-user btn-block">Aprovar</button></td>';
        linha += '</tr>';

        $('#grid-vagas-tabela').append(linha);
    }

    linha = '</tbody>';
    $('#grid-vagas-tabela').append(linha);
}

function pesquisarSolicitacaoProtocolo(protocolo) {
    $.ajaxSetup({ headers: { 'Authorization': 'Bearer ' + token } });
    $.getJSON(urlServicos + "api/private/v1/educacao/vaga/" + protocolo, function (result, status, xhr) {

        if (status == 'success') {
            montarGridProtocolo(result);

        } else {
            $('#tabela-vaga-protocolo tr').remove();
        }
    });
}

function montarGridProtocolo(solicitacao) {
    $('#tabela-vaga-protocolo tr').remove();

    linha = '<thead>';
    linha += '<tr>';
    linha += '<th>Escola</th>';
    linha += '<th>Endereço</th>';
    linha += '<th>Solicitante</th>';
    linha += '<th>Contato</th>';
    linha += '<th>Status</th>';
    linha += '</tr>';
    linha += '</thead>';
    linha += '<tbody>';
    linha += "<tr>";
    linha += '<tr>';
    linha += '<td>' + solicitacao.vaga.nome + '</td>';
    linha += '<td>' + solicitacao.vaga.logradouro + ', ' + solicitacao.vaga.bairro + ' - ' + solicitacao.vaga.cep + '</td>';
    linha += '<td>' + solicitacao.usuariosolicitacao.nome + '</td>';
    linha += '<td>' + solicitacao.usuariosolicitacao.telefone + '</td>';
    linha += '<td>' + solicitacao.status + '</td>';
    linha += '</tr>';
    linha += '</tbody>';

    $('#tabela-vaga-protocolo').append(linha);
}

function obterMinhasSolicitacoes() {
    $.ajaxSetup({ headers: { 'Authorization': 'Bearer ' + token } });
    $.getJSON(urlServicos + "api/private/v1/educacao/vaga", function (result, status, xhr) {

        if (status == 'success') {
            montarListaMinhasSolicitacoes(result);

        } else {
            $('#grid-vagas-tabela tr').remove();
        }
    });
}

function montarListaMinhasSolicitacoes(result) {
    let solicitacoes = result.solicitacoes;
    $('#grid-vagas-tabela tr').remove();

    linha = '<thead>';
    linha += '<tr>';
    linha += '<th>Escola</th>';
    linha += '<th>Endereço</th>';
    linha += '<th>Contato</th>';
    linha += '<th>Status</th>';
    linha += '</tr>';
    linha += '</thead>';

    $('#grid-vagas-tabela').append(linha);

    for (i = 0; i < solicitacoes.length; i++) {
        linha = '<tbody>';
        linha += "<tr>";
        linha += '<tr>';
        linha += '<td>' + solicitacoes[i].vaga.nome + '</td>';
        linha += '<td>' + solicitacoes[i].vaga.logradouro + ', '
            + solicitacoes[i].vaga.bairro + ' - ' + solicitacoes[i].vaga.cep + '</td>';

        linha += '<td>' + solicitacoes[i].vaga.responsavel + '</td>';
        linha += '<td>' + solicitacoes[i].status + '</td>';
        linha += '</tr>';

        $('#grid-vagas-tabela').append(linha);
    }

    linha = '</tbody>';
    $('#grid-vagas-tabela').append(linha);
}

function limparPesquisaProtocolo() {
    $('#numero-protocolo-solicitacao').val("");
    $('#tabela-vaga-protocolo tr').remove();
}




//INICIO - SERVICOS

function cadastrarNovoServico() {
    let servico = {
        descricao: $('#servico-nome').val(),
        valor: $('#servico-valor').val(),
        prazo_atendimento: $('#servico-prazo').val(),
        unidade_medida_prazo: $('#servico-unidade-prazo').val()
    }

    $.ajax({
        url: urlServicos + "api/administrative/v1/servico",
        method: "POST",
        dataType: "json",
        data: JSON.stringify(servico),
        headers: {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        },
        success: function (data) {
            $('#servico-nome').val("");
            $('#servico-valor').val("");
            $('#servico-prazo').val("");
            $('#servico-unidade-prazo').val("");

            inicializarTela();

        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Erro ao cadastrar vaga");
        }
    });
}

function obterServicosDisponiveis() {

    $.getJSON(urlServicos + "api/public/v1/servico", function (result, status, xhr) {
        montarListaServicos(result);
    });
}

function montarListaServicos(servicos) {
    grid = "SERVICOS";

    listaServicos = servicos.servicoLista;
    $('#grid-servicos-tabela tr').remove();
    linha = '<thead>';
    linha += '<tr>';
    linha += '<th>Servico</th>';
    linha += '<th>Valor</th>';
    linha += '<th>Prazo</th>';

    if (!isAdministrativo()) {
        linha += '<th></th>';
    }

    linha += '</tr>';
    linha += '</thead>';
    $('#grid-servicos-tabela').append(linha);

    for (i = 0; i < listaServicos.length; i++) {
        linha = '<tbody>';
        linha += "<tr>";
        linha += '<tr>';
        linha += '<td>' + listaServicos[i].descricao + '</td>';
        linha += '<td>' + listaServicos[i].valor + '</td>';
        linha += '<td>' + listaServicos[i].prazo + ' ' + listaServicos[i].unidade_prazo + '</td>';

        if (!isAdministrativo()) {
            linha += '<td><button value="' + listaServicos[i].id + '" class="btn btn-primary btn-user btn-block">Solicitar</button></td>';
        }

        linha += '</tr>';
        $('#grid-servicos-tabela').append(linha);
    }

    linha = '</tbody>';
    $('#grid-servicos-tabela').append(linha);
}

function obterSolicitacaoServicoPendente() {
    $.ajaxSetup({ headers: { 'Authorization': 'Bearer ' + token } });
    $.getJSON(urlServicos + "api/administrative/v1/servico/solicitacao/1", function (result, status, xhr) {
        console.log("servico: " + JSON.stringify(result));

        if (status == 'nocontent') {
            $('#grid_servicos tr').remove();

        } else {
            montarListaSolicitacaoServicoPendente(result);
        }
    });
}

function montarListaSolicitacaoServicoPendente(result) {
    grid = "SOLICITACOES_SERVICO";

    listaSolicitacoes = result.servicoLista;
    $('#grid_servicos tr').remove();

    linha = '<thead>';
    linha += '<tr>';
    linha += '<th>Protocolo</th>';
    linha += '<th>Serviço</th>';
    linha += '<th>Endereco</th>';
    linha += '<th>Solicitante</th>';
    linha += '<th>Contato</th>';
    linha += '<th>Status</th>';
    linha += '<th></th>';
    linha += '</tr>';
    linha += '</thead>';

    $('#grid-servicos-tabela').append(linha);

    for (i = 0; i < listaSolicitacoes.length; i++) {
        linha = '<tbody>';
        linha += '<tr>';
        linha += '<td>' + listaSolicitacoes[i].id + '</td>';
        linha += '<td>' + listaSolicitacoes[i].servico.descricao + '</td>';
        linha += '<td>' + listaSolicitacoes[i].endereco + ' - CEP ' + listaSolicitacoes[i].cep + '</td>';
        linha += '<td>' + listaSolicitacoes[i].usuarioSolicitacao.nome + '</td>';
        linha += '<td>' + listaSolicitacoes[i].usuarioSolicitacao.telefone + '</td>';
        linha += '<td>' + listaSolicitacoes[i].status + '</td>';
        linha += '<td><button value="' + listaSolicitacoes[i].id + '" class="btn btn-primary btn-user btn-block">Aprovar</button></td>';
        linha += '</tr>';

        $('#grid-servicos-tabela').append(linha);
    }

    linha = '</tbody>';
    $('#grid-servicos-tabela').append(linha);
}

function obterMinhasSolicitacoesServico() {

    $.ajaxSetup({ headers: { 'Authorization': 'Bearer ' + token } });
    $.getJSON(urlServicos + "api/private/v1/servico/solicitacao", function (result, status, xhr) {

        if (status == 'success') {
            montarListaMinhasSolicitacoesServico(result);

        } else {
            $('#grid-servicos-tabela tr').remove();
        }
    });
}

function montarListaMinhasSolicitacoesServico(result) {
    let solicitacoes = result.servicoLista;
    $('#grid-servicos-tabela tr').remove();

    linha = '<thead>';
    linha += '<tr>';
    linha += '<th>Protocolo</th>';
    linha += '<th>Serviço</th>';
    linha += '<th>Endereco</th>';
    linha += '<th>Responsável</th>';
    linha += '<th>Contato</th>';
    linha += '<th>Status</th>';
    linha += '</tr>';
    linha += '</thead>';

    $('#grid-servicos-tabela').append(linha);

    for (i = 0; i < solicitacoes.length; i++) {
        linha = '<tbody>';
        linha += '<tr>';
        linha += '<td>' + solicitacoes[i].id + '</td>';
        linha += '<td>' + solicitacoes[i].servico.descricao + '</td>';
        linha += '<td>' + solicitacoes[i].endereco + ' - CEP ' + solicitacoes[i].cep + '</td>';

        if (solicitacoes[i].responsavel == undefined) {
            linha += '<td></td>';
            linha += '<td></td>';

        } else {
            linha += '<td>' + solicitacoes[i].responsavel + '</td>';
            linha += '<td>' + solicitacoes[i].contato + '</td>';
        }

        linha += '<td>' + solicitacoes[i].status + '</td>';
        linha += '</tr>';

        $('#grid-servicos-tabela').append(linha);
    }

    linha = '</tbody>';
    $('#grid-servicos-tabela').append(linha);
}

function aprovarSolicitaoServico(id) {

    for (i = 0; i < listaSolicitacoes.length; i++) {
        if (listaSolicitacoes[i].id == id) {
            servicoSelecionado = listaSolicitacoes[i];

            $('#aprovar-servico-protocolo').val(listaSolicitacoes[i].id);
            $('#aprovar-servico-servico').val(listaSolicitacoes[i].servico.descricao);
            $('#aprovar-servico-solicitante').val(listaSolicitacoes[i].usuarioSolicitacao.nome);
            obterListaResponsavelServico(id);
        }
    }

    $('#grid_servicos').hide()
    $('#aprovar_novo_servico_form').show();
}

function obterListaResponsavelServico(id) {
    $.ajaxSetup({ headers: { 'Authorization': 'Bearer ' + token } });
    $.getJSON(urlServicos + "api/administrative/v1/servico/responsavel/" + id, function (result, status, xhr) {
        console.log("responsavel lista: " + JSON.stringify(result));

        if (status == 'success') {
            montarListaResponsavelServico(result);

        } else {
            alert('Não exite prestador viculado a este serviço');
        }
    });
}

function montarListaResponsavelServico(result) {
    $('#aprovar-servico-responsavel option').remove();
    listaResponsavel = result.responsaveis;

    let linha = '<option value="0">Selecione um responsável</option>';

    for (i = 0; i < listaResponsavel.length; i++) {
        linha += '<option value="' + listaResponsavel[i].id + '">' + listaResponsavel[i].nome + '</option>';

    }

    $('#aprovar-servico-responsavel').append(linha);
}

function solicitarServico(id) {
    $('#grid_servicos').hide();
    $('#solicitar_servico_form').show();

    for (i = 0; i < listaServicos.length; i++) {
        if (listaServicos[i].id == id) {
            servicoSelecionado = listaServicos[i];

            console.log("servico selecionado: " + JSON.stringify(servicoSelecionado));

            $('#servico-solicitacao-nome').val(servicoSelecionado.descricao);
            $('#servico-solicitacao-prazo').val(servicoSelecionado.prazo + ' ' + servicoSelecionado.unidade_prazo);
            $('#servico-solicitacao-valor').val(servicoSelecionado.valor);
        }
    }
}

function enviarSolicitacaoServico() {
    let solicitacao = {
        idservico: servicoSelecionado.id,
        endereco: $('#servico-solicitacao-endereco').val(),
        cep: $('#servico-solicitacao-cep').val(),
        observacao: $('#servico-solicitacao-observacao').val()
    }

    $.ajax({
        url: urlServicos + "api/private/v1/servico/solicitacao",
        method: "POST",
        dataType: "json",
        data: JSON.stringify(solicitacao),
        headers: {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        },
        success: function (data) {
            inicializarTela();

        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Erro ao aprovar solicitacao");
        }
    });
}

function pesquisarSolicitacaoProtocoloServico(protocolo) {
    $.ajaxSetup({ headers: { 'Authorization': 'Bearer ' + token } });
    $.getJSON(urlServicos + "api/private/v1/servico/solicitacao/" + protocolo, function (result, status, xhr) {
        console.log('servicos: ' + JSON.stringify(result));

        if (status == 'success') {
            montarGridProtocoloServico(result);

        } else {
            $('#tabela-servico-protocolo tr').remove();
        }
    });
}

function montarGridProtocoloServico(solicitacao) {
    $('#tabela-servico-protocolo tr').remove();

    linha = '<thead>';
    linha += '<tr>';
    linha += '<th>Protocolo</th>';
    linha += '<th>Serviço</th>';
    linha += '<th>Endereço</th>';
    linha += '<th>Solicitante</th>';
    linha += '<th>Contato</th>';
    linha += '<th>Status</th>';
    linha += '</tr>';
    linha += '</thead>';
    linha += '<tbody>';
    linha += "<tr>";
    linha += '<tr>';
    linha += '<td>' + solicitacao.id + '</td>';
    linha += '<td>' + solicitacao.servico.descricao + '</td>';
    linha += '<td>' + solicitacao.endereco + ' - CEP ' + solicitacao.cep + '</td>';
    linha += '<td>' + solicitacao.usuarioSolicitacao.nome + '</td>';
    linha += '<td>' + solicitacao.usuarioSolicitacao.telefone + '</td>';
    linha += '<td>' + solicitacao.status + '</td>';
    linha += '</tr>';
    linha += '</tbody>';

    $('#tabela-servico-protocolo').append(linha);
}

function limparPesquisaProtocoloServico() {
    $('#numero-protocolo-solicitacao-servicos').val("");
    $('#tabela-servico-protocolo tr').remove();
}

function aprovarSolicitacaoServico() {
    let idResponsavel = $('#aprovar-servico-responsavel :selected').val();

    let solicitacao = {
        idservicosolicitacao: servicoSelecionado.id,
        responsavel_id: 0,
        responsavel_nome: "",
        responsavel_telefone: ""
    }

    for (i = 0; i < listaResponsavel.length; i++) {
        if (listaResponsavel[i].id == idResponsavel) {
            solicitacao.responsavel_id = listaResponsavel[i].id;
            solicitacao.responsavel_nome = listaResponsavel[i].nome;
            solicitacao.responsavel_telefone = listaResponsavel[i].telefone;
        }
    }

    $.ajax({
        url: urlServicos + "api/administrative/v1/servico/solicitacao",
        method: "POST",
        dataType: "json",
        data: JSON.stringify(solicitacao),
        headers: {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        },
        success: function (data) {
            inicializarTela();

        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Erro ao aprovar solicitacao");
        }
    });
}

function isAdministrativo() {
    return perfil === 'ADMINISTRATIVE';
}

function isGeral() {
    return perfil === 'GENERAL';
}

function isSuport() {
    return perfil === 'SUPORT';
}